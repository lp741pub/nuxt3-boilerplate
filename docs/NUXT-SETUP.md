# Nuxt3 + Tailwind + ESLint + Pretty + Typescript
## SSR (Server Side Rendering)
 
1. ### Create Nuxt3 application (basic)
```bash
pnpm dlx nuxi init <project-name>
```
2. ### Install packages
```bash
pnpm install
```
3. ### Test configuration
```bash
pnpm dev
```
output
```text
> @ dev ...
> nuxt dev

                                                                                                                                                                                                                              10:58:10
Nuxi 3.0.0                                                                                                                                                                                                                    10:58:10
Nuxt 3.0.0 with Nitro 1.0.0                                                                                                                                                                                                   10:58:10
                                                                                                                                                                                                                              10:58:11
  > Local:    http://localhost:3000/ 
  ...

✔ Nitro built in 565 ms                                                                                                                                                                                                 nitro 10:58:14
ℹ Vite client warmed up in 2169ms                                                                                                                                                                                             10:58:15
ℹ Pages enabled. Restarting nuxt...  
```


### 4. Add Tailwind (basic)
```bash
pnpm i -D tailwindcss postcss autoprefixer
```
output
```
devDependencies:
+ autoprefixer ^10.4.13
+ postcss ^8.4.20
+ tailwindcss 3.2.4
```

### 5. Generate Tailwind config file
```bash
tailwind init
```
It generates `tailwind.config.js` in project root directory

then follow Tailwind [guide](https://tailwindcss.com/docs/guides/nuxtjs#3)

### 6. Configure static check Lint
```bash
pnpm i -D eslint \
 @typescript-eslint/eslint-plugin \
 @typescript-eslint/parser \
 eslint-plugin-nuxt \
 eslint-plugin-vue \
 typescript \
 stylelint \
 stylelint-scss \
 stylelint-config-recommended \
 stylelint-config-tailwindcss \
 sass
```

create `.eslintrc.js` in project root

```javascript
// .eslintrc.js 
module.exports = {
  root: true,
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    "plugin:@typescript-eslint/recommended",
    "plugin:nuxt/recommended",
    "plugin:vue/vue3-recommended",
  ],
  parserOptions: {
    ecmaVersion: "latest",
    parser: "@typescript-eslint/parser",
    sourceType: "module",
  },
  plugins: ["@typescript-eslint"],
  rules: {

  },
};
```

install vite plugin

```bash
pnpm i -D vite-plugin-eslint
```

add following code to `nuxt.config.ts`

```typescript
// nuxt.config.ts
import { defineNuxtConfig } from 'nuxt'
import eslintPlugin from 'vite-plugin-eslint';

export default defineNuxtConfig({
  vite: {
    plugins: [
      eslintPlugin()
    ]
  },
})
```
add lint scripts

```json
{
  "scripts": {
      "lint:js": "eslint --ext .js,.vue --ignore-path .gitignore .",
      "lint:style": "stylelint **/*.{vue,css} --ignore-path .styleignore",
      "lint": "pnpm lint:js && npm run lint:style"
  }
}
```

configure style lint create `.stylelintrc.json`

```json
{
  "extends": [
    "stylelint-config-recommended",
    "stylelint-config-tailwindcss/scss"
  ],
  "rules": {
    "at-rule-no-unknown": [ true, {
      "ignoreAtRules": [
        "extends",
        "tailwind"
      ]
    }],
    "block-no-empty": null,
    "unit-allowed-list": ["em", "rem", "s"]
  }
}
```

configure style lint create `.styleignore`

```text
.DS_Store
.idea
.rvmrc

# dependencies
node_modules

# logs
npm-debug.log

# Nuxt build
.nuxt

# Nuxt generate



*robots.txt
*sitemap.xml

```

create `.prettierrc`

```json
{
  "semi": false,
  "singleQuote": true
}
```

